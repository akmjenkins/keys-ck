<div class="mobile-nav-bg">&nbsp;</div>

<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav-wrap">
	<div class="nav">
		<div class="sw">
			
			<nav>
				<ul>
					<li><a href="#">Explore</a></li>
					<li>
					
						<a href="#">Why Spare Keys?</a>
						
						<div>
							<ul>
								<li><a href="#">Secure Storage</a></li>
								<li><a href="#">24 Hour Service</a></li>
								<li><a href="#">Guarantee</a></li>
								<li><a href="#">FAQs</a></li>
							</ul>
						</div>
						
					</li>
					<li><a href="#">Our Commitment</a></li>
					<li><a href="#">Plans &amp; Pricing</a></li>
					<li><a href="#">Login</a></li>
				</ul>
				
				<form action="/" method="get" class="single-form search-form">
					<fieldset>
						<input type="text" name="s" placeholder="Search Spare Keys...">
						<span class="close t-fa-abs fa-close">Close</span>
						<button class="t-fa-abs fa-search">Search</button>
					</fieldset>
				</form>
				
			</nav>
		
			<div class="nav-top">
				<a href="#">Reviews</a>
				<a href="#">Media</a>
				<a href="#">The Latest</a>
				<a href="#">Contact</a>
				
				<?php include('i-social.php'); ?>
				
			</div><!-- .nav-top -->
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->