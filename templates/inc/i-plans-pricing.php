			<div class="section-title">
				<h2 class="title">Plans &amp; Pricing</h2>
				<span class="subtitle">Lorem Ipsum Dolor</span>
			</div><!-- .section-title -->
			
			<div class="grid pad40 eqh">
				<div class="col col-2 sm-col-1">
					<div class="item pricing-panel">
					
						<div class="pricing-panel-title dark-bg">
							<span class="h3-style">Residential</span>
						</div><!-- .pricing-panel-title -->
						
						<span class="pricing-panel-price">
							<span>&nbsp;</span>
							$10 <small>per month</small>
						</span>
						
						<ul>
							<li>Lorem ipsum dolor sit amet</li>
							<li>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</li>
							<li>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</li>
							<li>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</li>
						</ul>
					
						<div class="pricing-panel-btns">
							<a href="#" class="button">Learn More</a>
							<a href="#" class="button blue">Purchase Now</a>
						</div>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 sm-col-1">
					<div class="item pricing-panel">
					
						<div class="pricing-panel-title dark-bg">
							<span class="h3-style">Commerical</span>
						</div><!-- .pricing-panel-title -->
						
						<span class="pricing-panel-price">
							<span>&nbsp;</span>
							$10 <small>per month</small>
						</span>
						
						<ul>
							<li>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</li>
							<li>Lorem ipsum dolor sit amet</li>
							<li>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</li>
						</ul>
						
						<div class="pricing-panel-btns">
							<a href="#" class="button">Learn More</a>
							<a href="#" class="button blue">Purchase Now</a>
						</div>
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->