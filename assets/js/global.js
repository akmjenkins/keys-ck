;(function(context) {

	$('.hero-wrap').each(function() {
		var el = $(this);
		var ctrls = $('.hero-nav .btn');
		var isHome = $('body').hasClass('home');
		var slickEl = el.slick({
			centerMode: true,
			slidesToShow:1,
			variableWidth:true,
			centerPadding:0,
			focusOnSelect: isHome,
			draggable:false,
			swipe:isHome,
			touchMove:isHome,
			autoplay: isHome,
			autoplaySpeed: 5000,
			pauseOnHover: false,
			responsive: [
				{
					breakpoint: 1024,
					variableWidth: false
				}
			],
			onBeforeChange: function(slick,e,i) {
				ctrls
					.removeClass('selected')
					.eq(i)
					.addClass('selected');
			}
		});
		
		ctrls
			.on('click',function() {
				slickEl.slickGoTo($(this).index());
			});
	});

	context.preventOverscroll($('div.nav')[0]);

}(window[ns]));