;(function(context) {

	var debounce = function() {
		this.isProcessing = false;
		this.method = null;
		this.methodScope = null;
	};
		
	debounce.prototype.requestProcess = function(method,scope) {
		if(!this.isProcessing) {
			this.method = method;
			this.methodScope = scope || window;
			this.isProcessing = true;
			
			requestAnimationFrame(function() {
			
				this.method.apply(this.methodScope);
				this.method = null;
				this.methodScope = null
				this.isProcessing = false;				
			
			}.bind(this));
		}
	};
	

	context.debounce = function() {
		return new debounce();
	}

}(window[ns]));